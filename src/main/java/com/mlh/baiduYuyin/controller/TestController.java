package com.mlh.baiduYuyin.controller;

import com.mlh.baiduYuyin.utils.Sample;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * @author:马立皓
 * @time:23:01 2023/7/18
 */
@Controller
@RequestMapping("/test")
public class TestController {

    @Resource
    private Sample sample;

    @RequestMapping("/testToSpeech")
    public void testToSpeech(){
        sample.synthesis(
                "起床：起床号（音频），各监室请注意，起床时间到了，所有人员迅速起床，叠好被子，排队洗漱，严禁拖沓，严禁扎堆洗漱，小红帽盯厕要到位。" +
                "起床号，各监室请注意，起床时间到了，请迅速起床，叠好被子，" +
                "排队洗漱，严禁拖沓，严禁扎堆洗漱，小红帽盯厕要到位。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\qichuang.wav"
        );
        sample.synthesis(
                "各监室请注意，早饭时间到了，请做好准备，" +
                "只能在自己铺位蹲下吃饭，严禁坐于床上，面朝电视方向，严禁伙吃伙喝。各监室请注意，早饭时间到了，" +
                "请做好准备，只能在自己铺位蹲下吃饭，严禁坐于床上，面朝电视方向，严禁伙吃伙喝。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\zaofan.wav"
        );
        sample.synthesis(
                "各监室请注意，放风时间到了，请做好出操准备，放风时严格落实群进群出制度，严禁监室内滞留人员，听到广播体操提示音或民警指令后，迅速站好，广播体操、" +
                "队列训练期间，所有在押人员必须全部到队列中参加，严禁无故不参加、搞特殊化、靠墙、嬉笑打闹等违规行为。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\fangfeng.wav"
        );
        sample.synthesis(
                "各监室请注意，正课时间到了，所有人员按规定坐姿做好，不准盘腿、翘二郎腿、把腿翘到床上，无假条不准非休息时间段随意躺卧。如上厕所，需小红帽报告民警同意后方可进行。各监室请注意，正课时间到了，" +
                "所有人员按规定坐姿做好，不准盘腿、翘二郎腿、" +
                "把腿翘到床上，无假条不准非休息时间段随意躺卧。如上厕所，需小红帽报告民警同意后方可进行。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\zhengke.wav"
        );
        sample.synthesis(
                "各监室请注意，课间活动时间到了，课间活动主要在个人铺位过道内伸展手臂，放松肌肉，不能做剧烈活动，" +
                "严禁躺卧、扎堆聊天、大声喧哗、到处乱走，严禁打牌、下棋等娱乐活动。各监室请注意，课间活动时间到了，课间活动主要在个人铺位过道内伸展手臂，" +
                "放松肌肉，不能做剧烈活动，严禁躺卧、扎堆聊天、大声喧哗、到处乱走，严禁打牌、下棋等娱乐活动。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\kejian.wav"
        );
        sample.synthesis("各监室请注意，午饭时间到了，请做好准备，只能在自己铺位蹲下吃饭，严禁坐于床上，面朝电视方向，" +
                "严禁伙吃伙喝。各监室请注意，早饭时间到了，请做好准备，" +
                "只能在自己铺位蹲下吃饭，严禁坐于床上，面朝电视方向，严禁伙吃伙喝。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\wufan.wav"
        );
        sample.synthesis(
                "各监室请注意，午休时间到了，在押人员进行午休，" +
                "值班在押人员迅速进入岗位，严禁大声喧哗。各监室请注意，午休时间到了，在押人员进行午休，" +
                "值班在押人员迅速进入岗位，严禁大声喧哗。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\wuxiu.wav"
        );
        sample.synthesis(
                "起床号（音频），各监室请注意，起床时间到了，" +
                "所有人员迅速起床，叠好被子，严禁拖沓。起床号，" +
                "各监室请注意，起床时间到了，所有人员迅速起床，叠好被子，严禁拖沓。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\wufanqichaung.wav"
        );
        sample.synthesis(
                "各监室请注意，晚间洗漱时间到了，所有人员必须排队洗漱，严禁扎堆，卫生间区域不能超过三人，" +
                "小红帽盯厕要到位。各监室请注意，晚间洗漱时间到了，所有人员必须排队洗漱" +
                "，严禁扎堆，卫生间区域不能超过三人，小红帽盯厕要到位。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\wanjianxishu.wav"
        );
        sample.synthesis(
                "各监室请注意，现在是晚点名时间，全体人员迅速集合，等待点名，严禁随意走动。各监室请注意，现在是晚点名时间，全体人员迅速集合，等待点名，严禁随意走动。各监室请注意，现在是晚点名时间，" +
                "全体人员迅速集合，等待点名，严禁随意走动。各监室请注意，" +
                "现在是晚点名时间，全体人员迅速集合，等待点名，严禁随意走动。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\wandainming.wav"
        );
        sample.synthesis(
                "各监室请注意，现在开始就寝，所有人员迅速打开被" +
                "子，立即就寝，小红帽抓紧时间到岗，严禁大声喧哗。各监室请注意，现在开始就寝，所有人员迅速打开被子，立" +
                        "即就寝，小红帽抓紧时间到岗，严禁大声喧哗。",
                "D:\\Project-packages\\baiduYuyin\\baiduYuyin\\jianyu\\jiuqing.wav"
        );

    }
}
