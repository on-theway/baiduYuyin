package com.mlh.baiduYuyin.utils;

import com.baidu.aip.speech.AipSpeech;
import com.baidu.aip.speech.TtsResponse;
import com.baidu.aip.util.Util;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

@Component
public class Sample {

    //设置APPID/AK/SK
    @Value("${voice.appid}")
    public String APP_ID;

    @Value("${voice.apikey}")
    public String API_KEY ;

    @Value("${voice.secretkey}")
    public String SECRET_KEY;

    public void synthesis(String text,String filePath) {
        // 初始化一个AipSpeec
        AipSpeech client = new AipSpeech(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
//        client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
//        client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理

        // 设置可选参数
        HashMap<String, Object> options = new HashMap<String, Object>();
        // 语速，取值0-15，默认为5中语速
        options.put("spd", 4);
        // 音调，取值0-15，默认为5中语调
        options.put("pit", 4);
        // 音量，取值0-15，默认为5中音量
        options.put("vol", 4);
        // 发音人选择, 基础音库：0为女声，1为男声，3为情感合成-度逍遥，4为情感合成-度丫丫，默认为普通女
        options.put("per", "1");
        // 下载的文件格式, 3：mp3(default) 4： pcm-16k 5： pcm-8k 6. wav
        options.put("aue", "6");
        TtsResponse res = client.synthesis(text, "zh", 1, options);
        System.out.println("返回信息："+res.getResult());//服务器返回的内容，合成成功时为null,失败时包含error_no等信息
        if(null == res.getResult()){
            byte[] data = res.getData();//生成的音频数据
            if (data != null) {
                try {
                    Util.writeBytesToFileSystem(data, filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
